function Function_handles = BBR_BW_get_predictions()
    Function_handles.main = @main;
    Function_handles.clean_data = @clean_data;
    Function_handles.check_flock_properties = @BBR_BW_check_flock_properties;
end

function [ final_matrix, output, output_avgweek, prediction_method, final_matrix_vars] = main( BW_rnd_flock, BW_rnd_flock_days, ynorm, ynorm_days, model)
 %Predictions of Broiler Breeder Rearing (Pullets) (BBR) Body Weight (BW), using mixed model (MM), percentage of
 % BW norm (PN), or BW norm (N)
 
 % INPUT:
 % BW_rnd_flock, BW_rnd_flock_days: Nx1 BW data and age
 % ynorm, ynorm_days: Mx1 BW norm data and age
 % model = MM model object
 
 %OUTPUT:
 % final_matrix: Nx5 matrix including all the BW prediction results 
 % output: Nx1 vector with the best prediction
 % prediction_method = string (MM,PN,PFE,N) which indicates the method used
 % for the predictions in the output
  
 %% Step 0: check inputs
assert(size(BW_rnd_flock,1)==size(BW_rnd_flock_days,1) && size(BW_rnd_flock,2)==1, 'Error: BW data and BW days should have equal length and one column')
TotalRowsBW = size(BW_rnd_flock,1);
Age = TotalRowsBW;

% if (sum(isnan(BW_rnd_flock))<Age && find(~(isnan(BW_rnd_flock)),1, 'last')==Age)
%     error('Breaking out of the BW_get_predictions function: the last data is at the end of flock life. There is nothing to predict.');
% end
%checking BW norm
% if (size(ynorm,1) < Age)
%     error(' The BW norm data in input is smaller than the lenght of flock life!');
% elseif (size(ynorm,1)> Age)
%     ynorm = ynorm(1:Age);
% end
if (sum(isnan(ynorm)) ~= 0 )
    warning(' The BW norm data in input have Nan values!');
end
if(size(ynorm,2)~=1)
    error(' The BW norm data are expected to be a one column array!');
end
[BW_rnd_flock] = clean_data(BW_rnd_flock,BW_rnd_flock_days,ynorm,ynorm_days);

%% Step 1: Set parameters
% Radjselec setting moved inside BBR_BW_check_flock_properties()

%% Step 2: Check flock properties
disp('Step 2: checking flock properties...');
%[pred] = BW_check_flock_properties(BW_rnd_flock,Radjselec,model,FE_rnd_flock,ynorm_fe,thr_FEnorm);
pred = BBR_BW_check_flock_properties(BW_rnd_flock,BW_rnd_flock_days,model);

%% Step 3: Get predictions 
disp('Step 3: getting predictions...');
ypred_mm = nan(size(BW_rnd_flock));
ypred_pn = nan(size(BW_rnd_flock));
lastwarn('');

%Method 1: Mix Model
if (pred(1) == 1)
    %[ ypred_mm,CIpred_mm] = BW_get_MM_predictions(D1,Epsilon,fix,ds_val,model,Age);
    [ypred_mm] = BW_get_MM_predictions(BW_rnd_flock,BW_rnd_flock_days,model);
    [warnmsg] = lastwarn;
    if (~isempty(warnmsg))
        pred(1) = 0;
        lastwarn('');
    end
end
%Method 2: Perc Norm
if (pred(2) == 1)
    %[ ypred_pn,CIpred_pn] = BW_get_PN_predictions(ds_val, ynorm, Age);   
    [ypred_pn] = BW_get_PN_predictions(BW_rnd_flock,BW_rnd_flock_days,ynorm,ynorm_days);
    [warnmsg] = lastwarn;
    if (~isempty(warnmsg))
        pred(2) = 0;
        lastwarn('');
    end
end

%% Step 4: Create final matrix: days, FE, BW, yhat, ypred_pn, ynorm, ypred_fe
disp('Step 4: creating final matrix...');

%disp(' final_matrix columns are: day, FE, BW, MMpred, MMlowCI, MMupCI, BWnorm, PNpred, PNlowCI, PNupCI, FEpred, FElowCI, FEupCI');
%final_matrix = horzcat([0:Age-1]',FE_rnd_flock,BW_rnd_flock, ypred_mm, CIpred_mm, ynorm, ypred_pn, CIpred_pn, ypred_fe, CIpred_fe);

ypred_no = arrayfun(@(d) ynorm(ynorm_days==d),BW_rnd_flock_days); % map norm days to BW days
final_matrix = horzcat(BW_rnd_flock_days,BW_rnd_flock, ypred_mm, ypred_pn, ypred_no);
final_matrix_vars = {'days', 'BW', '__predmm', '__predpn', '__predno'};
disp(' final_matrix has been created.')

%% create prediction Output
disp('Step 5: creating output matrix...');
if (pred(1) == 1)    
    disp(' the output contains the MM predictions.');
    output = horzcat(ypred_mm, nan(numel(ypred_mm),2));
    prediction_method = 'MM';
elseif (pred(2) == 1)
    disp(' the output contains the PN predictions.');
    output = horzcat(ypred_pn, nan(numel(ypred_pn),2));
    prediction_method = 'PN';
else
    disp(' the output contains the norm.');
    output = horzcat(ypred_no, nan(numel(ypred_no),2));
    prediction_method = 'N';   
end
disp(' output columns are: pred, lowCI, upCI'); %lowCI, upCI = NaN's
disp(' output has been created.');

%output not the daily BW but the weekly average of the daily BW -every day
%the average of the previous seven days.
disp('output_avgweek is average week')
days_per_week = 7;
is_valid_output = ~isnan(output(:,1));
for d = BW_rnd_flock_days(:)'
    pos = (BW_rnd_flock_days > (d - days_per_week)) & (BW_rnd_flock_days <= d) & is_valid_output;
    output_avgweek(d == BW_rnd_flock_days,:) = mean(output(pos,:),1);
end
disp(' output_avgweek columns are: pred, lowCI, upCI');
disp(' output_avgweek has been created.');
end % BBR_BW_get_predictions

%% CLEANING
function [BW_rnd_flock] = clean_data(BW_rnd_flock,BW_rnd_flock_days,ynorm,ynorm_days) 
%Clean BW data by removing negative & zero values, and norm-based outliers

    disp(' Negative BW data are set to NaN');
    BW_rnd_flock(find(BW_rnd_flock<0))  = NaN; %negative BW does not make sense
    disp(' Zero BW data are set to NaN');
    BW_rnd_flock(find(BW_rnd_flock==0)) = NaN; %BW = 0 does not make sense
    
    % remove outliers from the norm
    % get norms at BW days
    norm = interp1(ynorm_days,ynorm,BW_rnd_flock_days,'linear',nan); % nan - return Nan's outside norm days
    index_outlier = isoutlier(BW_rnd_flock, norm, 3);
    BW_rnd_flock(index_outlier) = nan;
end

function [ index_outlier ] = isoutlier(data, norm, factor_tot )
if nargin < 3
    fact = 6;
else fact = factor_tot;
end
X = data-norm(1:size(data,1));
%X = FE;
mk = median(X(~isnan(X)));
M_d = mad(X,1);
c = -1/(sqrt(2)*erfcinv(3/2));
smad = c*M_d;
tsmad = fact*smad;
index_outlier = (abs(X-mk)>tsmad);
end

%% CHECK FLOCK
 function pred = BBR_BW_check_flock_properties(BW_rnd_flock,BW_rnd_flock_days,model)
 % Check the properties of the flock to decide which method using for the prediction
 % INPUT:
 % BW_rnd_flock  = BW data for the flock under prediction
 % Radjselec     = minimum R2adjsted obtained with the MM model
 % model         = MM model object
 %
 %OUTPUT: 
 % pred          = 1x2 = [MM,percNorm], 1 or 0 if the model can be used or not.  
 %
 % (See also BW_check_flock_properties() for the original script and check of FE data)
  
Radjselec = 0.75; % value to check if the prediction can be done with MM
disp([' Radjselec = ', num2str(Radjselec)]);
 
pred = [0,0];

val_flock = 0;
num_obs = sum(~isnan(BW_rnd_flock));

%checking if the PN model can be used
days = find(~(isnan(BW_rnd_flock))); % at least one value is neeed
if ((size(days,1)>0))
        disp(' For this flock, the Percentage Norm Model can be used');
        pred(2) = 1;
end 

% checking if the MM can be used
if (num_obs > 5)
    [BW_new,BW_days] = BBR_BW_filter_data(BW_rnd_flock,BW_rnd_flock_days,Radjselec,model);
    val_flock = length(BW_new); 
end
% if (val_flock ~= 0 || (num_obs > 0 && num_obs <= 5 && pred(2)==1)) % Double check the 2nd condition !!!!!!!!
if (val_flock ~= 0) 
    disp(' For this flock, the Mix Model can be used');
    pred(1) = 1;
end
end

 %% CHECK MM FIT
 function [BW_new,BW_days] = BBR_BW_filter_data(BW_rnd_flock,BW_rnd_flock_days,Radjselec,model)
 %Data filter for daily body weight data
 %The data filter
 %
 % INPUT:
 % Radjselec= minimum Radj value is needed to be selected for the selection
 % as reference data .9
 %
 %OUTPUT:
 % BW_new= is the body weight data in one array ready for mixed model
 % BW_days= the respectively days coupled to the BW registrations in BW_new
 % 
 % See also the original BW_filter_data()

pos = ~isnan(BW_rnd_flock);
x = BW_rnd_flock_days(pos);
y = BW_rnd_flock(pos);
xpred = x;
ypred = model.predict(xpred,x,y);          

res = y - ypred;
ymean = mean(y);
ymean_array(1:length(y),1)=ymean;
%RMSE = sqrt((1/(length(days)))*sum(res.^2));
Radj=1-((length(y)-1)/(length(y)-5))*(sum(res.^2)/sum((y-ymean_array).^2)); 
%R2 shows how well terms (data points) fit a curve or line.
%Adjusted R2 also indicates how well terms fit a curve or line, but adjusts for the number of terms in a model:
%-if you add more and more useless variables to a model, adjusted r-squared will decrease.
%-if you add more useful variables, adjusted r-squared will increase. 
%Adjusted R2 will always be less than or equal to R2.   

if (Radj >= Radjselec)
    %creating BW_new and BW_days                
    BW_new  = y; 
    BW_days = x;
else
    BW_new  = []; 
    BW_days = [];
end
 end
 
%% MM PREDICITONS
function [ypred_mm] = BW_get_MM_predictions(BW,BW_days,model)
% Predict BW for each day in BW_days, using all past data in BW to adjust the MM model. 

pos = ~isnan(BW);
x_fit = BW_days(pos);
y_fit = BW(pos);
ypred_mm = model.predict(BW_days,x_fit,y_fit); 
end
 
 %% PERCENTAGE NORM PREDICTION
function [ypred_pn] = BW_get_PN_predictions(BW,BW_days,ynorm,ynorm_days)
 %Predict Body Weight after last valid value in input BW until the last BW_days, based on Percentage Norm. 
 %
 % INPUT:
 % BW, BW_days: Nx1 Body Weight data and age for which we want prediction
 % ynorm, ynorm_days = BW norm data and age
 
 %OUTPUT:
 % y_npred: Nx1 prediction according to the norm for all days after the last day where there is BW data AND norm data
 %
 % See also: BW_get_PN_predictions() for the original code and calculation of 95% CI
 
 ypred_pn = BW*nan;
 ypred_pn_days = BW_days;
 Age = max(BW_days);
 
 % remove NaN's from BW
 pos = ~isnan(BW); 
 BW = BW(pos);
 BW_days = BW_days(pos);
 
 % remove NaN's from norm
 pos = ~isnan(ynorm); 
 ynorm = ynorm(pos);
 ynorm_days = ynorm_days(pos);
  
 % calculate BW - norm
 num = BW(ismember(BW_days,ynorm_days)) - ynorm(ismember(ynorm_days,BW_days));
 den = ynorm(ismember(ynorm_days,BW_days));

 y_npred_days = intersect(max(BW_days)+1:Age,ynorm_days);
 for dd = y_npred_days'
     correction = sum(num./den .* ( 1 ./ ( dd - BW_days(ismember(BW_days,ynorm_days)))))/sum( 1 ./ ( dd - BW_days(ismember(BW_days,ynorm_days))));
     y_npred = ynorm(ynorm_days==dd)* (1 + correction);

     ypred_pn(ypred_pn_days==dd) = y_npred;
 end 
end