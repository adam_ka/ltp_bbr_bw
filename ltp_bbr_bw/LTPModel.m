classdef LTPModel
    methods
        function obj = LTPModel(varargin)
        end % function
        function model = save(obj,file)
            s = obj.to_struct();
            save(file,'-struct','s');
        end % function
        function s = to_struct(obj,s)
            if nargin==1
                s = struct();
            end % if
            s.model = class(obj);
        end % function
    end % methods
    methods(Static)
        function varargout = parse_option(key,args,value)
            values = args(2:2:end);
            varargout = [values(strcmpi(key,args(1:2:end-1))) {value}];
        end % function
        function obj = load(file)
            s = load(file);
            obj = LTPModel.from_struct(s);
        end % if
        function obj = from_struct(s,obj)
            if nargin==1
                obj = feval(s.model);
                obj = obj.from_struct(s,obj);
            end % if            
        end % function
    end % methods(Static)
end % classdef