% get norms

%% Load data
AGE = 'age_days';
GROUP = 'flockID';
CLIENT = 'client';
DESCRIPTOR = 'rowNum';

datafolder = '..\data\tyson_broiler_breeder_pullets2_20201218'
%{ 
Params available in 8 flocks from tyson_bbr as of Dec 17, 2020:
'bre_bwavg_fama_hoor_00_fe_00dd00_00'
'bre_bwavg_fama_hoor_00_fe_00wwav_00' % repeated values of dd
'bre_bwavg_fama_hoor_00_ma_00dd00_00'
'bre_bwavg_fama_hoor_00_ma_00wwav_00' % repeated values of dd
+ norms
%}

% load norm
norm = 'BW_F';
PAR  = 'bre_bwavg_fama_hoor_00_fe_00dd00_00';
data = PRODs2table(datafolder,AGE,{norm,PAR},GROUP,CLIENT,DESCRIPTOR);

% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));
num_units = numel(group_name);

Function_handles = LI_SG_BBR_BW_get_predictions();
get_predictions = Function_handles.main;
model_struct = load('BW_pul_tyson_model_test.mat');
model = LTPModel.from_struct(model_struct);
age_today = 50;

for k = 1:num_units
    flock = group==k;
    BW_rnd_flock_days   = data.(AGE)(flock);
    BW_rnd_flock        = data.(PAR)(flock);
    ynorm_days          = data.(AGE)(flock);
    ynorm               = data.(norm)(flock);
    
    %[ final_matrix, output, prediction_method] = get_predictions( BW_rnd_flock, BW_rnd_flock_days, ynorm, ynorm_days, model, age_today)
    [ final_matrix, output, output_avgweek, prediction_method, final_matrix_vars] = get_predictions( BW_rnd_flock, BW_rnd_flock_days, ynorm, ynorm_days, model);
    final_matrix
end