classdef LTPGompertzMortality < LTPLinearMixedModel
    methods
        function obj = LTPGompertzMortality(varargin);
            obj = obj@LTPLinearMixedModel('design',@(x) [ones(numel(x),1) x(:)]);
        end % function
        function varargout = fit(obj,x,y,g)
            [xt,yt] = obj.transform(x,y,g);
            [varargout{1:nargout}] = fit@LTPLinearMixedModel(obj,xt,yt,g);
        end % function
        function [y,par] = predict(obj,x,varargin)
            if nargin==4 % Random effects fit
                ind = find(isfinite(varargin{1}) & isfinite(varargin{2}),1,'last');
                xa = varargin{1}(ind);
                ya = varargin{2}(ind);
                [varargin{1},varargin{2}] = obj.transform(varargin{1},varargin{2});
            elseif nargin==2 % Fixed effects fit
                xa = x(1);
                ya = 0;
            else
                error('Predict requires 2 or 4 non-empty input arguments!');
            end % if
            [y,par] = predict@LTPLinearMixedModel(obj,x,varargin{:});
            par = par(1:2)+par(3:4); % Add fixed- & random effects together
            par(end+1) = log(100-ya)+exp(par(1)+par(2)*xa)/par(2);
            y = 100-exp(par(end)-exp(y)/par(2));
        end % function
    end % methods
    methods(Static)
        function [x,y,g] = transform(x,y,g)
            % TRANSFORM applies the log-hazard transformation to inputs (x,y), preserving
            % their size & ordering.
            if nargin==3
                [name,~,ind] = unique(g);
                for k = 1:numel(name)
                    group = ind==k;
                    [x(group),y(group)] = LTPGompertzMortality.transform(x(group),y(group));
                end % for
                return
            elseif nargin~=2
                error('Transform requires 2 or 3 input arguments');
            end % if
            valid = find(isfinite(x) & isfinite(y) & y<100);
            if numel(valid)<2 % Need at least 2 points to calculate derivative
                y(:) = nan;
                return
            end % if
            [~,sorted] = sort(x(valid));
            sorted = valid(sorted);
            dy = -diff(log(100-y(sorted)))./(diff(x(sorted)));
            dy = (dy([1 1:end])+dy([1:end end]))/2;
            dy(dy<=0) = nan;
            y(:) = nan;
            y(sorted) = log(dy);
        end % function
    end % methods(Static)
end % classdef