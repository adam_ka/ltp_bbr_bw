function data_out = PRODs2table(datafolder,AGE,PAR,GROUP,CLIENT,DESCRIPTOR)
% PRODS2TABLE Load PROD files into single Matlab table
%   INPUTS:
%       PAR - parameter name, or cell array of parameter names, for selection from PROD files.
%             Return NaN values if a param. is not in the PROD file. 

if ~iscell(PAR)
    PAR = {PAR}
end
prodfiles = dir([datafolder '\*_Prod.txt']);
data = table();
for i = 1:length(prodfiles)
    filename = [datafolder '\' prodfiles(i).name];
    %ds = dataset('file',filename);
    T = readtable(filename,'ReadVariableNames',true,'Delimiter','tab');
    if ~ismember('age_days',T.Properties.VariableNames)
        if ~ismember('Days',T.Properties.VariableNames)
            warning([filename ' has no age_days or Days colums. Skipping the Prod file!'])
            continue
        else
            T.age_days = T.Days;
        end
    end
    for p = PAR(:)'
        if ~ismember(p,T.Properties.VariableNames)
            T.(p{:}) = nan(size(T,1),1);
        end
    end
    
    Tselect = T(:,{AGE, PAR{:}});
    Tselect.(GROUP) = repmat({prodfiles(i).name},size(Tselect,1),1);
    Tselect.(DESCRIPTOR) = repmat({num2str(i)},size(Tselect,1),1);
    Tselect.(CLIENT) = repmat({'_'},size(Tselect,1),1);
    data = [data; Tselect];
end
data_out = data;
end