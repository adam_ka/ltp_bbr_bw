function filename = exportToPPTX_appendFigures(...
    fileName_str,...
    figHandles_array,...
    slideTexts_cellarray,...
    slideNotes_cellarray)
% Append to ppt 'fileName', figure(s).

if length(fileName_str)==0
    return
end

%% Start new presentation
isOpen  = exportToPPTX();
if ~isempty(isOpen),
    % If PowerPoint already started, then close first and then open a new one
    exportToPPTX('close');
end

try
    exportToPPTX('open',fileName_str);
catch
    exportToPPTX('new','Dimensions',[11 8.5]);
end

%% Add some slides
for islide=1:length(figHandles_array),
    figH = figHandles_array(islide);
    slideNum = exportToPPTX('addslide');
    fprintf('Added slide %d\n',slideNum);
    exportToPPTX('addpicture',figH,'Scale','maxfixed');
    exportToPPTX('addtext',slideTexts_cellarray{islide});
    exportToPPTX('addnote',slideNotes_cellarray{islide});
end

%% Save presentation and close presentation -- overwrite file if it already exists
% Filename automatically checked for proper extension
newFile = exportToPPTX('saveandclose',fileName_str);
