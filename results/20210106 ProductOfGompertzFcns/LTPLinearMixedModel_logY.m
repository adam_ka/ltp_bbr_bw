classdef LTPLinearMixedModel_logY
    properties
        model = struct('design',@(x) [ones(numel(x),1) x(:)]);
        FIT_METHOD = 'ML';
    end % properties
    methods
        function obj = LTPLinearMixedModel_logY(varargin)  % function obj = LTPLinearMixedModel(varargin) <<<<<<<<<<<<<<<<<<<<<<<< AK
            if nargin
                if isstruct(varargin{1})
                    obj.model = varargin{1};
                else
                    varargin(1:2:end) = lower(varargin(1:2:end));
                    obj.model = struct(varargin{:});
                end % if
            end % if
            obj.model.name = mfilename();
        end % function
        function [obj,ds] = train(obj,x,y,g)
            if exist('table')
                ds = table();
            else
                ds = dataset(); % Compatibility with R2013a
            end % if
            in = isfinite(x) & isfinite(y) & isfinite(g);
            ds.x = x(in,:);
            % ds.y = y(in);
            ds.y = log(y(in)); % <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< AK
            ds.g = g(in);
            C = obj.model.design(ds.x);
            formula = '-1'; % design matrix C should have intercept, so don't add here.
            for k = 1:size(C,2)
                name = ['x' num2str(k)];
                ds.(name) = C(:,k);
                formula = [formula '+' name];
            end % for
            formula = ['y~' formula '+(' formula '|g)'];
            lme_obj = fitlme(ds,formula,'FitMethod',obj.FIT_METHOD);
            covariance = covarianceParameters(lme_obj);
            obj.model.covariance = covariance{1};
            obj.model.epsilon = var(residuals(lme_obj));
            obj.model.fixed_effects = fixedEffects(lme_obj);
            if nargout>1 % Output table with training data (for debugging or visualization).
                %ds.yhat = fitted(lme_obj,'Conditional',true);
                %ds.yhat_fixed = fitted(lme_obj,'Conditional',false);
                ds.yhat = exp(fitted(lme_obj,'Conditional',true)); % <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< AK
                ds.yhat_fixed = exp(fitted(lme_obj,'Conditional',false)); % <<<<<<<<<<<<<<<<<<<<<<<<<<< AK
                ds.y = exp(ds.y); % <<<<<<<<<<<<<<<<<<<<<<<<<<< AK
            end % if
        end % function
        function obj = fit(obj,x,y)
            C = obj.model.design(x);
            DC = obj.model.covariance*C';
            Z = obj.model.epsilon*eye(size(C,1));
            % obj.model.random_effects = DC*inv(C*DC+Z)*(y-C*obj.model.fixed_effects); % Verbeke
            obj.model.random_effects = DC*inv(C*DC+Z)*(log(y)-C*obj.model.fixed_effects); % Verbeke <<<<<<<<<<<<<<<<<<<<< AK
        end % function
        function yhat = predict(obj,x)
            %yhat = obj.model.design(x)*(obj.model.fixed_effects+obj.model.random_effects);  
            yhat = exp(obj.model.design(x)*(obj.model.fixed_effects+obj.model.random_effects));  % <<<<<<<<<<<<<<<<<<<<< AK
        end % function        
    end % methods
end % classdef