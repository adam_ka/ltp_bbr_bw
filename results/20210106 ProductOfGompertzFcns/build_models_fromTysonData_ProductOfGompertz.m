function build_models()
% Build and test linear mixed effects model for broiler breeder rearing body weight (BW) parameter
%
% tested with MATLAB R2016a

% INPUT
raw_data_file_fe = '../data/broiler_breeder_pullets_bw_fe_dd_2020_09_11.csv'; %50 flocks
raw_data_file_ma = '../data/broiler_breeder_pullets_bw_ma_dd_2020_09_11.csv'; %32 flocks
raw_data_file_2_table = @bbr_csv_2_table

% Aliases for (frequently used) fields in data
AGE = 'age_days';
AGE_DISPLAY_NAME = 'Age (days)';
PAR = 'BW';
NORM = PAR; % select actual norm name when using norm-based outlier detection in BBR_BW_get_predictions()
PAR_DISPLAY_NAME = 'BW (g)';
GROUP = 'flockID';
CLIENT = 'client';
DESCRIPTOR = 'rowNum';

% OUTPUT
model_name = 'LTPLinearMixedModel_logY';

%{ 
% Diphasic Gompertz - manual fit A (with constant term)
Gompertz1 = @(t) exp(-exp(-0.0879*1*(t/7-0/7)));
Gompertz2 = @(t) exp(-exp(-0.0879*3*(t/7-130/7)));
model_args =  {'design',@(x) [ones(numel(x),1) Gompertz1(x(:)) Gompertz2(x(:))]}  % 
ppt_filename = 'bbr_LTP_BW_Female_GompertzDiphasic.pptx';
%}

%{ 
% Diphasic Gompertz - nlmefit without constant term & starting from manual fit A
Gompertz1 = @(t) exp(-exp(-0.50841*(t/7-14.203/7)));
Gompertz2 = @(t) exp(-exp(-0.077319*(t/7-155.88/7)));
model_args =  {'design',@(x) [Gompertz1(x(:)) Gompertz2(x(:))]} 
ppt_filename = 'bbr_LTP_BW_Female_GompertzDiphasic.pptx';
%}


%%{ 
% Product of Gompertz - manual fit A (with constant term)
Gompertz1 = @(t) -exp(-0.0879*1*(t/7-0/7));
Gompertz2 = @(t) -exp(-0.0879*3*(t/7-100/7));
model_args =  {'design',@(x) [ones(numel(x),1) Gompertz1(x(:)) Gompertz2(x(:))]}  % 
ppt_filename = 'bbr_LTP_BW_Female_GompertzDiphasic.pptx';
%}

%%{ 
% Product of Gompertz curves - manual fit A
Gompertz1 = @(t) -exp(-0.0879*20*(t/7-0/7));
Gompertz2 = @(t) -exp(-0.0879*1*(t/7-0/7));
model_args =  {'design',@(x) [ones(numel(x),1) Gompertz1(x(:)) Gompertz2(x(:))]}  % 
ppt_filename = 'bbr_LTP_BW_Female_GompertzDiphasic.pptx';
%0.0879 * [10, 1](50,-190) , [20,1](60,-90), [30,1](70,-50),
%[40,1](78,-30), [50,1](80,-30), [100,1](80,-30)

%}



model_version = 'test'; %datestr(now(),'yyyy-mm-dd');
model_file = ['ltp_bbr_bw_' model_version '.mat'];

% OPTIONS
max_age_days = 150 %113 days in norm
max_training_days = inf; % max num of days from age_today into the past used for flock prediction
min_fit_range = 7*0; % min num of days from age_today into the past used for single flock prediction
min_obs = 6; % This is a technical limitation of the model fitting, so should be moved there?

Age_today = (16:-4:4)*7;
Line_color = {'r','g','b','m'};
xlim = [0 max_age_days];

% Initialize PPTX export
if exist(ppt_filename,'file')
    delete(ppt_filename);
end % if

%% Start new presentation
try
    isOpen = exportToPPTX();
catch % add exportToPPTX toolbox to path
    addpath('exportToPPTX');
    isOpen = exportToPPTX();
end % try
    
if ~isempty(isOpen),
    % If PowerPoint already started, then close first and then open a new one
    exportToPPTX('close');
end
try
    exportToPPTX('open',fileName_str);
catch
    exportToPPTX('new','Dimensions',[11 8.5]);
end

%% Read training data
data_fe = raw_data_file_2_table(raw_data_file_fe); 
data_ma = raw_data_file_2_table(raw_data_file_ma);
data_fe.flockID = cellfun(@(x) [x '_fe'],data_fe.flockID, 'UniformOutput', false);
data_ma.flockID = cellfun(@(x) [x '_ma'],data_ma.flockID, 'UniformOutput', false);
data = [data_fe];
%data = [data_ma];

%% Load 2nd data set

datafolder = '..\data\tyson_broiler_breeder_pullets2_20201218'

%{
Params available in 8 flocks from tyson_bbr as of Dec 17, 2020:
'bre_bwavg_fama_hoor_00_fe_00dd00_00'
'bre_bwavg_fama_hoor_00_fe_00wwav_00' % repeated values of dd
'bre_bwavg_fama_hoor_00_ma_00dd00_00'
'bre_bwavg_fama_hoor_00_ma_00wwav_00' % repeated values of dd
%}


PAR2 = 'bre_bwavg_fama_hoor_00_fe_00dd00_00';% 'BW_F'; %
NORM2 = 'BW_F';
PAR2_DISPLAY_NAME = PAR2;
data2 = PRODs2table(datafolder,AGE,{PAR2,NORM2},GROUP,CLIENT,DESCRIPTOR);

%%{
% clean data
Fcns_get_predictions = BBR_BW_get_predictions();
for k = unique(data2.(GROUP))'
    flock = strcmp(data2.(GROUP),k);
    x = data2.(AGE)(flock);
    y = data2.(PAR2)(flock);
    ynorm = data2.(NORM2)(flock);
    ynorm_days = data2.(AGE)(flock);
    [y,x] = Fcns_get_predictions.clean_data(y,x,ynorm,ynorm_days);
    data2.(AGE)(flock) = x;
    data2.(PAR2)(flock) = y;
end
%} 

% remove NaN and negative age
data2 = data2(~isnan(data2.(PAR2)),:);
data2 = data2(data2.(AGE)>0,:);

%% Set Dataset #1 to Tyson data
data = data2;
PAR = PAR2;
NORM = NORM2;
PAR_DISPLAY_NAME = PAR2_DISPLAY_NAME;

%% Adjust multiphasic model params
%{
mp_model = @(PHI,t) PHI(1)*exp(-exp(-PHI(2)*(t/7-PHI(3)/7))) + PHI(4)*exp(-exp(-PHI(5)*(t/7-PHI(6)/7))) %+ PHI(7) not converging with constant term
PHI0 = [1000 0.0879 0 1000 0.0879*3 130];
[group_name,group_start,group] = unique(data.(GROUP));
PHI1 = nlmefit(data.(AGE),data.(PAR),group,[],mp_model,PHI0,'RefineBeta0',false) 
%}

%{
mp_model = @(PHI,t) PHI(1)*exp(-exp(-PHI(2)*(t/7-PHI(3)/7)) - exp(-PHI(4)*(t/7-PHI(5)/7)) )  %not converging with constant term
PHI0 = [7.5 0.0879 0 0.0879*3 130];
[group_name,group_start,group] = unique(data.(GROUP));
PHI1 = nlmefit(data.(AGE),data.(PAR),group,[],mp_model,PHI0,'RefineBeta0',true) 
%}


%% Train model
% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));

model = feval(model_name,model_args{:});
[model,ds] = model.train(data.(AGE),data.(PAR),group);
model_struct = model.model;
save(model_file,'-struct','model_struct');

h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
line(ds.x,ds.y,'marker','+','linestyle','none','DisplayName','data');
line(ds.x,ds.yhat,'marker','o','linestyle','none','color','g','DisplayName','fit with random effects');
line(ds.x,ds.yhat_fixed,'marker','*','linestyle','none','color','r','DisplayName','fit with fixed effects only');
title([PAR_DISPLAY_NAME ' model'],'interpreter','none');
legend('show');
set(gca,'xgrid','on','ygrid','on');
%ylim = [min([ds.y; ds.yhat; ds.yhat_fixed]) max([ds.y; ds.yhat; ds.yhat_fixed])];
%set(gca,'xlim',xlim,'ylim',ylim,'xgrid','on','ygrid','on');

slideNum = exportToPPTX('addslide');
fprintf('Added slide %d\n',slideNum);
exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
exportToPPTX('addtext','Linear Mixed Model training');
exportToPPTX('addnote','');

%% Test model with 1st dataset
data = testing( model,...
                data,...
                [],...
                AGE,AGE_DISPLAY_NAME,PAR,NORM, PAR_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim);
            
prediction_performance_summary( data,...
                                Age_today,...
                                AGE,AGE_DISPLAY_NAME,PAR,PAR_DISPLAY_NAME,...
                                max_training_days,Line_color,xlim,...
                                model)
                           
%% Testing on 2nd dataset
data2 = testing(model,...
                data2,...
                BBR_BW_get_predictions(),...
                AGE,AGE_DISPLAY_NAME,PAR2, NORM2, PAR2_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim);

prediction_performance_summary( data2,...
                                Age_today,...
                                AGE,AGE_DISPLAY_NAME,PAR2,PAR2_DISPLAY_NAME,...
                                max_training_days,Line_color,xlim,...
                                model)


%% Save presentation and close presentation -- overwrite file if it already exists
% Filename automatically checked for proper extension
newFile = exportToPPTX('saveandclose',ppt_filename);

end

function data = bbr_csv_2_table(raw_data_file)
% Read data into table flockID,age_days,BW
    data0 = readtable(raw_data_file);
    data = table();
    for i = 1:height(data0)
        row = data0(i,:);
        BW = row{1,2:end}';
        age_days = (1:length(BW))';
        rowNum = repmat({num2str(i)},length(BW),1); %string
        flockID = repmat(row{1,1},length(BW),1); %string
        client = repmat({'_'},length(BW),1);
        data = [data; table(rowNum,flockID,age_days,BW,client)];
    end
    % remove NaN
    data = data(~isnan(data.BW),:);
end

%%  plot prediction performance summary
function prediction_performance_summary(data,...
                                        Age_today,AGE,AGE_DISPLAY_NAME,PAR,PAR_DISPLAY_NAME,...
                                        max_training_days,Line_color,xlim,...
                                        model)
for p = 1:3
    h_fig(p) = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
    h_ax(p) = axes();
end % for
for j = 1:length(Age_today)
    ageToday = Age_today(j);
    try
        pred_error = data.([PAR '_pred_' num2str(j)]) - data.(PAR);
    catch
        continue
    end % try
    in = isfinite(pred_error);
    [age,~,age_grp] = unique(data.(AGE)(in));
    mean_measure = grpstats(data.(PAR)(in),age_grp,@mean);
    [mean_pred_error,std_pred_error,num_pred] = grpstats(pred_error(in),age_grp,{'mean','std','numel'});
    disp_name = ['Fit=[' num2str(ageToday-max_training_days) ',' num2str(ageToday) '['];
    LineWidth = 2;
    MarkerSize = 8;
    line(h_ax(1),age,mean_measure,'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' mean']);
    line(h_ax(2),age,mean_pred_error,'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' error mean']);
    patch(h_ax(2),[age;flipud(age)],[mean_pred_error+std_pred_error;flipud(mean_pred_error-std_pred_error)],...
          Line_color{j},'DisplayName',[disp_name ' error std'],'EdgeColor','none','FaceAlpha',0.1);
    line(h_ax(3),age,num_pred,'linestyle','-','LineWidth',LineWidth,'marker','*','MarkerSize',MarkerSize,'color',Line_color{j},'DisplayName',[disp_name ' num']);
end % for
for p = 1:3
    xlabel(h_ax(p),AGE_DISPLAY_NAME);
    title(h_ax(p),['PARAMETER = ' PAR_DISPLAY_NAME ', MODEL = ' model.model.name ': ' func2str(model.model.design)],'interpreter','none');
    set(h_ax(p),'xlim',xlim,'xgrid','on','ygrid','on'); %set(h_ax(p),'xlim',xlim,'xtick',0:70:xlim(2),'xgrid','on','ygrid','on');
    legend(h_ax(p),'show','Location','northwest');
end % for
ylabel(h_ax(1),PAR_DISPLAY_NAME,'interpreter','none');
ylabel(h_ax(2),['Prediction Error = Pred - ' PAR_DISPLAY_NAME],'interpreter','none');
%set(h_ax(2),'ylim',[-20 20]);
ylabel(h_ax(3),'Number of flocks');
fig_text = {'Average measurement','Prediction error distribution','Number of data points'};
for p = 1:3
    slideNum = exportToPPTX('addslide');
    fprintf('Added slide %d\n',slideNum);
    exportToPPTX('addpicture',h_fig(p),'Scale','maxfixed');
    exportToPPTX('addtext',fig_text{p});
    exportToPPTX('addnote','');
end % for

end

%%
function data = testing(model,...
                        data,...
                        Fcns_get_predictions,...
                        AGE,AGE_DISPLAY_NAME,PAR,NORM,PAR_DISPLAY_NAME, GROUP, CLIENT, DESCRIPTOR,...
                        max_age_days,max_training_days,min_obs,min_fit_range,Age_today,Line_color,xlim)
%model = feval(lme.model_cmd,lme.model_args{:});
%model_name = lme.model_name;

% Convert string flock id to numeric flock id
[group_name,group_start,group] = unique(data.(GROUP));
client_name = data.(CLIENT)(group_start);
descriptor = data.(DESCRIPTOR)(group_start);
num_units = numel(group_name);

h_fig = figure('units','normalized','outerposition',[0.05 0.05 0.9 0.9]);
for j = 1:length(Age_today)
    data.([PAR '_pred_' num2str(j)]) = nan(size(data,1),1);
end % for

for k = 1:num_units
    %flock = group==k & isfinite(data.(PAR)) & data.(PAR)>=0 & data.(PAR)<100 & data.(AGE)<=max_age_days;
    flock = group==k & isfinite(data.(PAR)) & data.(AGE)<=max_age_days;
    x = data.(AGE)(flock);
    y = data.(PAR)(flock);
    ynorm = data.(NORM)(flock);
    ynorm_days = data.(AGE)(flock);
    min_x = min(x);
    max_x = max(x);

    has_pred = false;    
    clf(h_fig);
    h_ax = axes();
    line(x,y,'linestyle','none','marker','*','MarkerSize',8,'color','k','DisplayName','Data');
    for j = find(Age_today>min_x+min_fit_range & Age_today<=max_x)
        ageToday = Age_today(j);
        fit_range = [max(min_x,ageToday-max_training_days) ageToday];
        fit = x>=fit_range(1) & x<fit_range(2);
        x_fit = x(fit);
        if numel(x_fit)<min_obs
            continue
        end % if
        has_pred = true;
        y_fit = y(fit);  
        
        prd = x>=ageToday;
        yhat = nan(size(y));
        
        if isempty(Fcns_get_predictions)
            model = model.fit(x_fit,y_fit);
        else
            % clean data and check flock properties
            [y_fit,x_fit] = Fcns_get_predictions.clean_data(y_fit,x_fit,ynorm,ynorm_days);
            pos = ~isnan(y_fit);
            x_fit = x_fit(pos);
            y_fit = y_fit(pos);
            model = model.fit(x_fit,y_fit);
            yhat_fit = model.predict(x_fit);
            pred = Fcns_get_predictions.check_flock_properties(y_fit,x_fit,model);
            if pred(1)==0
                continue
            end
        end
        yhat(prd) = model.predict(x(prd));
        data.([PAR '_pred_' num2str(j)])(flock) = yhat;

        disp_name = ['Fit=[' num2str(fit_range(1)) ',' num2str(fit_range(2)) '['];
        data_disp_name = [disp_name ' Data'];
        pred_disp_name = [disp_name ' Pred'];
        p = 1; %:2 % 1 = full prediction, 2 = after ageToday only
        %line(h_ax(p),x_fit,y_fit,'linestyle','none','marker','*','color',Line_color{j},'DisplayName',data_disp_name);
        LineWidth = 2;
        line(h_ax(p),max_x:max_age_days,model.predict(max_x:max_age_days),'linestyle',':','LineWidth',LineWidth,'marker','none','color',Line_color{j},'HandleVisibility','off'); % show future predictions beyond available data
        line(h_ax(p),fit_range(2):max_x,model.predict(fit_range(2):max_x),'linestyle','-','LineWidth',LineWidth,'marker','none','color',Line_color{j},'DisplayName',pred_disp_name); % show future predictions within available data
        line(h_ax(1),fit_range(1):fit_range(2),model.predict(fit_range(1):fit_range(2)),'linestyle','--','LineWidth',LineWidth,'marker','none','color',Line_color{j},'HandleVisibility','off'); % show model fitting to the data up to age today
    end % for
    ylim = [-5 5*(1+ceil(max(y)/5))];
    p = 1;%:2
    xlabel(h_ax(p),AGE_DISPLAY_NAME)
    ylabel(h_ax(p),PAR_DISPLAY_NAME,'interpreter','none')
    title(h_ax(p),[PAR_DISPLAY_NAME ' model = ' model.model.name],'interpreter','none');
    set(h_ax(p),'xlim',xlim,'xgrid','on','ygrid','on'); %set(h_ax(p),'xlim',xlim,'ylim',ylim,'xtick',0:70:xlim(2),'xgrid','on','ygrid','on');
    legend(h_ax(p),'show','Location','northwest');
    if has_pred
        slideNum = exportToPPTX('addslide');
        fprintf('Added slide %d\n',slideNum);
        exportToPPTX('addpicture',h_fig,'Scale','maxfixed');
        exportToPPTX('addtext',['CLIENT:' client_name{k} ' UNIT:' group_name{k} ' DESCRIPTOR:' descriptor{k} ' MODEL:' model.model.name]);
        exportToPPTX('addnote','');
    end % if
end % for
end
